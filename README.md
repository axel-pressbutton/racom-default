# README #

This is a demo of a Drupal Installation Profile that I'm tinkering with.

It'll download Drupal 7 core and a few modules that I like to use when I create a fresh basic Drupal 7 install and enable them.

It will also set the default admin theme to 'Shiny'.

### Modules and Themes ###

A list of modules and themes used can be found in the `racom.make` file. Everything that gets enabled can be found in the `racom.info` file.

Configuration is carried out in the `racom.install` file. The following example will run a standard Drupal install, set the default admin theme to 'Shiny' and disable the default admin theme that ships with Drupal 7, 'Seven'

```php
function racom_install() {
  include_once DRUPAL_ROOT . '/profiles/standard/standard.install';
  standard_install();

  // Enable the admin theme.
  $admin_theme = 'shiny';
  theme_enable(array($admin_theme));
  variable_set('admin_theme', $admin_theme);
  variable_set('node_admin_theme', '1');

  // Disable the old theme.
  theme_disable(array('seven'));
}
```